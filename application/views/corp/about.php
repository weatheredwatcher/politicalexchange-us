<?php $this->load->view('_header'); ?>


</head>

<body id="sub">
	<!-- Here is the code for talking heads video -->
	<script type="text/javascript" src="http://www.politicalexchange.us/wthvideo/wthvideo4.js"></script>
	<!-- End of Talking Heads -->
<!-- Starting the Upper Half - - Blue Area -->

<div id="flagInside"></div>

<div id="wrapperInside">

<div id="contentwrapInside">
	

    	<div id="logoInside">
           <div id="politician">
        <?=$this->load->view('_topmenu');?></div>
        </div>
   
		
    <div id="columnwrap">
    
    
   		  <div id="colRight">
   		  		
				<div id="ads">
			         <img src="images/ad2.png" alt="" width="280" />
			         <img src="images/ad3.png" alt="" width="280" />
			         <img src="images/ad1.png" alt="" width="280" />
			    </div>

   		  </div>
                
    
        
             
   			 <div id="colLeft">
           	   <div id="contentColLeft">
           	   	<div id="topProfile">
               	  <h2> About Us </h2>
                  
				<div id="representatives">
						<p>The idea for PX began during a local Sheriff’s race in South Carolina in the Summer of 2009. 
						One candidate had the desire to connect with every eligible voter, but due to an ever-growing 
						electorate, the ability to reach each voter depended largely on the size of a campaign account. 
						This candidate realized the potential of internet and social media for affordably connecting a 
						candidate to his or her constituency, thus leveling the “playing field” and allowing every voter 
						to truly know candidates running for public office. </p>
						<p>That idea was shared with a few close friends, all of whom steadfastly believe in real political 
						transparency. After almost two years of hard work and planning, the idea became reality. Political 
						Exchange is now the only place every candidate in every race in America can go to directly connect 
						with every voter. PX is also the only place voters can go to truly see who the candidates are and 
						what they really believe. </p>

						<p>FAMILY:	We at Political Exchange have a strong belief in family. Family is at the very Core of 
						our Country. Most of us have already begun building families of our own. Our workplace environment 
						is structured so that we provide premium customer service to every candidate or constituent member, 
						as well as strengthen and nurture every employee’s own family. We think of each other as family and 
						treat each other as such. We gladly extend an open invitation, into the PX family, to everyone who 
						desires and demands truth and freedom of ideas in the political process of Our Republic.</p>   
			
					<div id="clear"> </div>
					</div>
                </div>

               	 <div id="divide"></div>
                <div id="clear"></div>
                <div id="middleProfile">



               </div>

           	   </div>
        
  </div>






</div>
<div id="clear"></div>
</div>

<div id="clear"></div>

 <?php echo $this->load->view('_footer'); ?></div>



<!-- End Blue  -->


</body>
</html>

