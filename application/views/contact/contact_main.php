<?php $this->load->view('_header'); ?>


</head>

<body>

<!-- Starting the Upper Half - - Blue Area -->
<div id="black">
<div id="flag"></div>

<div id="wrapper">

<div id="contentwrap">
	
    <div id="stars">
    	<div id="logo"><img src="images/logo.png"  alt="Political Exchange" /><br />
    	
    	</div>
    	<div id="location" ><!-- Begin MailChimp Signup Form -->
<link href="http://cdn-images.mailchimp.com/embedcode/slim-081711.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:transparent; clear:left; font:14px Helvetica,Arial,sans-serif;color: #FFFFFF;text-align:center;}
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
 <h2 style="color:#FFFFFF; font-family:Tahoma, Geneva, sans-serif;font-weight:normal;margin-bottom:3px;">Contact Us!</h2>
<form action="http://politicalexchange.us2.list-manage.com/subscribe/post?u=b975c4d16dbe8fcad821bb7f8&amp;id=3ab61eb48e" method="post"  name="mc-embedded-subscribe-form" class="validate" target="_blank">
	<p style="font-size:15px;font-weight: bold;margin-top:0px;">
		
		<br />
	 Want to drop us a note?  Please show your interest by sending us a request for contact and we will get in touch with you as quickly as possible!</p>
	<p align="center"><input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required></p>
	<div class="clear" align="center"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
</form>
</div>

<!--End mc_embed_signup--></div>
    
        
        
    
    
    </div>


</div>

</div>
</div>

<!-- End Blue  -->

<!-- Starting the lower half (Blue AREA) -->
<div id="clear"></div>
<div id="bottom">

<div id="bottomwrap"></div>
<div id="clear"></div>
</div>

<?php echo $this->load->view('_footer'); ?>
</body>
</html>
